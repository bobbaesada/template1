# Example

It is a good idea to have a README.md file in each directory describing what is contained in the directory.

## What this project does

This project is a template. It has one script that prints Hello and all the arguments passed to it

## Purpose

This is to be used as a basis and template for other projects
