# Summary

This project is a template for other projects.
You can modify it to create your own projects.  This README.md file can be modified, for example,
to describe what your project does, how to use your project, how to build your project,
point to additional materials such as source code, documentation and build scripts, 
how the project was designed and put together, any limitations or bugs, any future enhancements,
or other comments.

# How to use it

Please use it as basis for your own projects.  Try to follow the structure of this template as much as possible.  You may find that in a very simple project, if the question being asked is very simple, you may not need all of the directories. For example, the  `build` directory may not be relevant, if you are just writing a very simple bash script.  You can try to be as thorough as possible. But being reasonable is the key.  Not too much, not too little.


# What this template project contains

At top level, this README.md file.
And there should be a `run.sh` or `run.bat` file which can be run without any arguments from a shell to provide all necessary input and produce clear output that demonstrate that the project does what it claims to do.

## Source code 

Under `sources` directory. Should contain all the code and necessary scripts and other materials that implement the project.

## Data

Under `data` directory, if the project uses data in a certain format, with certain content, and assumptions. Provide example input data.

## Documentation

Under `documentation` directory. Describe the project, what it does, how it works, how to build and run the project, what are the tests
that have been run in what way, how it is designed and what bugs and limitations exist for the future enhancements, and any additional comments.

## Tests

Under `tests` directory.  Any test scripts and programs that show that the program functions well.
This directory should have a `test.sh` or `test.bat` script.
Unit tests, system tests, integration tests, etc. should be here as needed.

## Build

Under `build` directory.   Any build scripts, Dockerfiles, Makefiles, ant, maven, cargo, npm, sbt, gradle, etc. files.
The build output can be placed in a separate sub directory of your own choosing. For example `build/output/windows` or `build/output/linux` directories may be used.  Or in the case of scripts that do not need to be built at all, no such directories are needed, but perhaps  you can package them. For example, you may package into a format that can be uploaded to `npm` or python repositories.
